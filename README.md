# Nodejs CI/CD demo on OpenShift

Create a new project on OpenShift. A project is loosely comparable to `namespace` in Kubernetes with security enhanced RBAC.

```bash
oc new-project test-project --display-name="Test Project" --description="An OpenShift CI/CD demo using nodejs app."
```

### Helm
Tested with helm-v3-alpha.

```bash
helm install -name nodejs-openshift-cicd ./helm
```

To check CI, modify welcome message in `src/app.js` and push the change.

### Manually build
```bash
oc start-build nodejs-openshift-cicd --follow
```

### Private repo
OpenShift needs access credentials to build and deploy application from private git repos. Generate an auth token. On bitbucket web UI navigate to **Your profile and settings > Bitbucket settings > App passwords > Create app password**.
Generated token looks like `f5JfVJZgvCU9xK5T8XA4`.

Create a Kubernetes secret of type `kubernetes.io/basic-auth`. SSH auth can be used too. See more at https://blog.openshift.com/private-git-repositories-part-1-best-practices/.


```bash
oc create secret generic bitbucket-auth-secret \
    --from-literal=username=hmoazzem \
    --from-literal=password=f5JfVJZgvCU9xK5T8XA4 \
    --type=kubernetes.io/basic-auth
```

Run `oc secrets link` to allow the builder service account to use it.

```bash
oc secrets link builder bitbucket-auth-secret
```
Next, annotate the secret with the URI for the repository.

```bash
oc annotate secret/bitbucket-auth-secret \
    'build.openshift.io/source-secret-match-uri-1=https://hmoazzem@bitbucket.org/hmoazzem/nodejs-openshift-cicd.git'
```

`oc new-app` command builds up the components of an application. If application source code is specified, the code must be in a git repo (if local repo, it must have a remote).

```bash
oc new-app https://hmoazzem@bitbucket.org/hmoazzem/nodejs-openshift-cicd.git \
    --source-secret=bitbucket-auth-secret
```

See logs
```bash
oc logs -f bc/nodejs-openshift-cicd --follow
```

Expose the app

```bash
oc expose svc/nodejs-openshift-cicd --hostname=nodejs.openshift.local
```

Append `nodejs.openshift.local` hostname entry in `/etc/hosts` file.

```bash
echo -e "\n$(minishift ip)\tnodejs.openshift.local" | sudo tee -a /etc/hosts
```

Try with pipeline. Create the pipeline build controller from the openshift/pipeline subdirectory.
```bash
oc new-app https://hmoazzem@bitbucket.org/hmoazzem/nodejs-openshift-cicd.git \
    --source-secret=bitbucket-auth-secret \
    --context-dir=openshift/pipeline --name nodejs-openshift-cicd-pipeline
```


# OpenShift: Pros and Cons
OpenShift adds tools on top of Kubernetes to enable faster application development, easy deployment and scaling. OpenShift aims to avoid the need for additional components after the initial rollout. It comes with **proprietary Ansible-based installer**, which can install OpenShift with minimal configuration parameters. OpenShift comes with an integrated image registry. OpenShift has a native networking solution out-of-the-box, OpenvSwitch, which offers three different plugins.

Pros:
- RBAC
- CRD (Project, BuildConfig, Route, etc)
- Built in starters
- Easy Eclipse Che integration
- CoreOS base image
- Regular k8s objects
- OpenShift DSL for Jenkins
- Container security (immutable config)
- Multi tenancy in cluster
- Pipeline not particularly integrated with Jenkins
- Tekton

Cons:
 - Works only with docker containers


